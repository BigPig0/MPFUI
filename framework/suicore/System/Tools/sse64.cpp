
#include <Windows.h>

extern "C" {

void ___DoXorSSE(DWORD dwKey, char * pBuff, int nSSELen)
{
}

void ___MemCopySSE(LPVOID pDest, LPVOID pSrc, int nSSESize)
{
}

void ___MemSetDWordSSE(LPVOID pBuff, DWORD dwVal, int nSSESize)
{	
}

void ___DoGraySSE(LPVOID pBuff, int nSSESize)
{
    DWORD dwMask = 0xff;
    DWORD dwDiv3 = 85;
}


void ___OpenAlphaSSE(LPVOID pBuff, int nSizeSSE)
{
    DWORD dwMask = 0xff000000;
}

void ___SetColorKeySSE(LPVOID pBuff, int nSSESize, DWORD dwColor)
{
    DWORD dwMask = 0xffffff;
}


void ___DoOrSSE(DWORD dwKey, LPVOID pBuff, int nSSELen)
{
}

void ___DoAndSSE(DWORD dwKey, char * pBuff, int nSSELen)
{
}

}

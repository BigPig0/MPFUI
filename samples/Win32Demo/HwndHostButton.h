#pragma once
#include <Framework/Controls/HwndHost.h>

using namespace suic;

class HwndHostButton : public HwndHost
{
public:
	static DpProperty* ContentProperty;
	static DpProperty* IDProperty;

	static void StaticInit();

	HwndHostButton(void);
	~HwndHostButton(void);

	RTTIOfClass(HwndHostButton)

protected:
	void OnLoaded(LoadedEventArg* e);
	void OnUnloaded(LoadedEventArg* e);

private:
	HWND m_Handle;
	HFONT m_font;
};


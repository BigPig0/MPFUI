#pragma once

class CModItemData : public NotifyPropChanged
{
public:
	BeginMember(CModItemData, NotifyPropChanged)
		MemberString(Name)
	EndMember()
	DefineString(Name)
};

class CModManager
{
public:
	CModManager();
	~CModManager();
	void ScanMods();

	ObservableCollection* m_pModCollection;
};

#include "stdafx.h"
#include "MainWindow.h"
#include "UIHelp.h"
#include "Ra2ModTool.h"

suic::InvokeProxy* CMainWindow::_reflesh = NULL;

CMainWindow::CMainWindow()
	:m_pCenterControl(NULL)
	,m_pPlayBox(NULL)
{
	m_pListModsData = new ObservableCollection();
	m_pListModsData->ref();
}

CMainWindow::~CMainWindow()
{
	m_pListModsData->unref();
}

bool CMainWindow::OnBuild(suic::IXamlNode* pNode, suic::ObjectPtr& obj)
{
    return false;
}

void CMainWindow::OnConnect(suic::IXamlNode* pNode, suic::Object* target)
{
    
}

void CMainWindow::OnInitialized(suic::EventArg* e)
{
    suic::Window::OnInitialized(e);

	//Invoke初始化
	_reflesh = new suic::InvokeProxy(this);
	_reflesh->ref();
	_reflesh->Invoker+=suic::InvokerHandler(this, &CMainWindow::OnInvoker);

	//Mod仓库页面-mod列表
	suic::ListBox* listMods = FindElem<suic::ListBox>(_U("listMods"));
	if (NULL != listMods)
	{
		m_modManager.ScanMods();
		listMods->SetItemsSource(m_modManager.m_pModCollection);

		listMods->AddSelectionChanged(new SelectionChangedEventHandler(this, &CMainWindow::OnListBoxSelChanged));
	}

	//中间详细信息
	m_pCenterControl = FindElem<suic::StackPanel>(_U("CenterInfo"));
}

void CMainWindow::OnLoaded(suic::LoadedEventArg* e)
{
    suic::Window::OnLoaded(e);

    // 注册按钮事件处理
    RegisterButtonEvent();
    
}

bool CMainWindow::OnEvent(Object* sender, MessageParam* mp)
{
	if (mp->message == WM_COMMAND)
	{
		if (10001 == mp->wp)
		{
			return true;
		}
		else if (10002 == mp->wp)
		{
			return true;
		}
		else if (10003 == mp->wp)
		{
			return true;
		}
		else if (20001 == mp->wp)
		{
			return true;
		}
		return false;
	}
	else
	{
		return false;
	}
}

void CMainWindow::RegisterButtonEvent()
{
	// 注册路由事件
	Window* win = DynamicCast<Window>(FindName(_U("window")));
	if (NULL != win)
	{
		win->AddHandler(Button::ClickEvent, new ClickEventHandler(this, &CMainWindow::OnButtonClicked));
	}
}

void CMainWindow::OnInvoker(suic::Object* sender, suic::InvokerArg* e)
{
	suic::UIGuard<suic::Mutex> sunc(m_mutex);
	int nWhat = e->GetWhat();
	Object* pObj = e->GetData();
	/*
	if (WM_USER_ADDTASK == nWhat)
	{
		InvokeParam* pParam = (InvokeParam*)pObj;
		CTaskData* pData = (CTaskData*)(pParam->ptr);
		
		CListboxItemData* newItemData = new CListboxItemData();
		newItemData->SetID(pData->m_nID);
		newItemData->SetInfo1(pData->m_strInfo1.c_str());
		newItemData->SetInfo2(pData->m_strInfo2.c_str());
		newItemData->SetInfo3(pData->m_strInfo3.c_str());
		AddNewItem(newItemData);
		pParam->unref();
	}
	else if(WM_USER_REMOVE == nWhat)
	{
		InvokeParam* pParam = (InvokeParam*)pObj;
		int* nPhoneID = (int*)(pParam->ptr);
		RemoveItem(*nPhoneID);
		pParam->unref();
	}
	else if(WM_USER_RUNNING == nWhat)
	{
		InvokeParam* pParam = (InvokeParam*)pObj;
		CTaskData* pData = (CTaskData*)(pParam->ptr);
		UpdateItem(pData, TaskState_Running);
		pParam->unref();
	}
	else if (WM_USER_WATING == nWhat)
	{
		InvokeParam* pParam = (InvokeParam*)pObj;
		CTaskData* pData = (CTaskData*)(pParam->ptr);
		UpdateItem(pData, TaskState_Waiting);
		pParam->unref();
	}
	else if(WM_USER_INPUT == nWhat)
	{
		InvokeParam* pParam = (InvokeParam*)pObj;
		CTaskData* pData = (CTaskData*)(pParam->ptr);
		UpdateItem(pData, TaskState_Input);
		pParam->unref();
	}
	else if(WM_USER_CANCEL == nWhat)
	{
		InvokeParam* pParam = (InvokeParam*)pObj;
		CTaskData* pMsgNode = (CTaskData*)(pParam->ptr);
		UpdateItem(pMsgNode, TaskState_Home);
		pParam->unref();
	}
	*/
}

void CMainWindow::OnButtonClicked(Element* sender, RoutedEventArg* e)
{
	Element* btn = DynamicCast<Element>(e->GetOriginalSource());
	if(btn == nullptr)return;

	suic::Object* pItem = btn->GetDataContext();
	CListboxItemData* pItemData = (CListboxItemData*)pItem;
	if(pItemData == nullptr)return;

	String strName = btn->GetName();
	if (strName.Equals(_U("UserInfo_OK")))		//用户信息填写完成
	{
		pItemData->SetTaskState(TaskState_Finish);
		e->SetHandled(true);
	}
	else if (strName.Equals(_U("Reset"))|| strName.Equals(_U("Return")))	// 重置
	{
		pItemData->SetTaskState(TaskState_Home);
		pItemData->Reset();
		e->SetHandled(true);
	}
}

void CMainWindow::OnListBoxSelChanged(Element* sender, SelectionChangedEventArg* e)
{
	String str = sender->GetName();
	int nSel = e->GetAddedItems()->GetCount();
	if (nSel > 0)
	{
		Object* p = e->GetAddedItems()->GetItem(0);
		CModItemData* pData = RTTICast<CModItemData>(p);

		//if (NULL != m_pCenterControl && NULL != pData)
		//{
		//	m_pCenterControl->SetDataContext(pData);
		//}
	}
	e->SetHandled(true);
}
#include "framework.h"
#include "ModManager.h"
#include <string>
#include <filesystem>
#include <iostream>

using namespace std;

CModManager::CModManager()
{
    m_pModCollection = new ObservableCollection();
}

CModManager::~CModManager()
{
    m_pModCollection->unref();
}

void CModManager::ScanMods()
{
    // 获取当前程序所在路径
    wchar_t buffer[MAX_PATH];
    GetModuleFileNameW(nullptr, buffer, MAX_PATH); // 获取 exe 完整路径
    std::filesystem::path currentPath = std::filesystem::path(buffer).parent_path();

    // 指定要扫描的文件夹名称
    std::string folderName = "mods"; // 替换为你的文件夹名称
    std::filesystem::path targetFolder = currentPath / folderName;

    // 检查目标文件夹是否存在
    if (!std::filesystem::exists(targetFolder) || !std::filesystem::is_directory(targetFolder)) {
        std::cerr << "文件夹不存在或不是一个有效的目录: " << targetFolder << std::endl;
        return;
    }

    std::cout << "扫描文件夹: " << targetFolder << std::endl;
    std::cout << "子文件夹列表:" << std::endl;

    // 遍历目标文件夹
    for (const auto& entry : std::filesystem::directory_iterator(targetFolder)) {
        if (std::filesystem::is_directory(entry)) { // 检查是否是文件夹
            std::cout << entry.path().filename() << std::endl;
            CModItemData* item = new CModItemData();
            item->SetName(entry.path().filename().wstring().c_str());
            m_pModCollection->AddItem(item);
        }
    }
}